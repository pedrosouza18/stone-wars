import Home from './routes/Home.html';
import Character from './routes/Character.html';
import ListCharacter from './routes/ListCharacters.html';

export default {
  '/': Home,
  '/character': Character,
  '/list': ListCharacter,
};
