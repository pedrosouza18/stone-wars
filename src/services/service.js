import Http from '@mamba/pos/api/http.js';
import { API_URL } from './api.js';

export default class Service {
  requestObj(endpoint) {
    return {
      url: endpoint,
      headers: {
        'Content-Type': 'application/json;charset=UTF-8',
      },
      method: 'GET',
      proxy: true,
    };
  }

  async _getHomeWorld(endpointHome) {
    const requestHome = this.requestObj(endpointHome);
    const resultHome = await Http.send(requestHome);
    return JSON.parse(resultHome.body);
  }

  async _getItem(endpointItem) {
    const requestItem = this.requestObj(endpointItem);
    const result = await Http.send(requestItem);
    return JSON.parse(result.body);
  }

  async _asyncForEach(array, callback) {
    const results = [];
    for (let index = 0; index < array.length; index++) {
      results.push(callback(array[index]));
    }
    await Promise.all(results);
  }

  async getItemsCharacter(items) {
    const arrayItems = [];
    await this._asyncForEach(items, async item => {
      const result = await this._getItem(item);
      arrayItems.push(result);
    });
    return arrayItems;
  }

  getCharacter() {
    const randomNumber = Math.floor(Math.random() * 87 + 1);
    const myRequest = this.requestObj(`${API_URL}${randomNumber}/`);
    return new Promise((resolve, reject) => {
      Http.send(myRequest)
        .then(result => {
          const characterObj = { ...JSON.parse(result.body) };
          if (characterObj.homeworld) {
            this._getHomeWorld(characterObj.homeworld).then(response => {
              characterObj.homeworld = response.name;
            });
          }
          resolve(JSON.parse(result.body));
        })
        .catch(error => reject(error.msg));
    });
  }
}
